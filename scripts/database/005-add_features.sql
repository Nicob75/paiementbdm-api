use paiement_bdm;

CREATE TABLE features
(
    `Id`              INT       NOT NULL AUTO_INCREMENT,
    `Name`            VARCHAR(64) NULL,
    `HtmlDescription` TEXT NULL,
    `CreatedDate`     timestamp NOT NULL,
    PRIMARY KEY (`Id`)
);

CREATE TABLE userreadfeatures
(
    `Id`        INT    NOT NULL AUTO_INCREMENT,
    `UserId`    BIGINT NOT NULL,
    `FeatureId` INT    NOT NULL,
    PRIMARY KEY (`Id`),
    INDEX       `FK_UserReadFeatures_UserId_idx` (`UserId` ASC),
    INDEX       `FK_UserReadFeatures_FeatureId_idx` (`FeatureId` ASC),
    CONSTRAINT `FK_UserReadFeatures_UserId`
        FOREIGN KEY (`UserId`)
            REFERENCES `users` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION,
    CONSTRAINT `FK_UserReadFeatures_FeatureId`
        FOREIGN KEY (`FeatureId`)
            REFERENCES `features` (`Id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
);
