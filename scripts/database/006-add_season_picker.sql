use paiement_bdm;
SET @current_season = 10;

-- create our appconfig table to store general app configutation
CREATE TABLE IF NOT EXISTS `app_parameters` (
                                  `app_key` varchar(64) DEFAULT NULL,
                                  `app_value` varchar(256) DEFAULT NULL,
                                  UNIQUE KEY `key_UNIQUE` (`app_key`)
) ENGINE=InnoDB;

-- create the CURRENT_SEASON parameter and initialize it
INSERT INTO `app_parameters`
VALUES ("CURRENT_SEASON", @current_season);
