SELECT * FROM eventplayers ep
LEFT JOIN events ev on ep.EventId = ev.Id
WHERE ev.SeasonId = 7
-- 	AND ep.CreatedBy = 'Cedric'
ORDER BY ep.LastModifiedDate DESC;

SELECT * FROM eventplayers ep
LEFT JOIN events ev on ep.EventId = ev.Id
LEFT JOIN players pl on pl.Id = ep.PlayerId
WHERE ev.SeasonId = 7
	AND pl.Firstname = 'Nicolas'
    AND pl.Lastname = 'BARTHELEMY'
--     AND ep.Payment <> 0
ORDER BY ep.LastModifiedDate DESC;
