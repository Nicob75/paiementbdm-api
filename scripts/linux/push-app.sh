#!/bin/bash
# Push the Docker image
# command line arguments :
# APP_ENV : the desired environment => not used, but required because of DOCKER_IMAGE_TAG argument
# (optional) DOCKER_IMAGE_TAG the docker image tag
# example : push-app.sh uat (paiement-bdm-api)

# Load the environment variables
source ./load-app-env.sh $*
if ! [ $? -eq 0 ]; then
  echo "ERROR : An error occured while loading app environment. Aborting."
  pause
  exit $?
fi

source ./change-dir.sh
echo "Current directory: $(pwd)"

# Push the Docker image
docker image push --tag=$DOCKER_IMAGE_TAG .
if ! [ $? -eq 0 ]; then
  echo "ERROR : Error while running docker push. Aborting."
  pause
  exit 1103
fi
