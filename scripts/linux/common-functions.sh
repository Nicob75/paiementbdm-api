#!/bin/bash
# You can move your function definitions into a separate file
# and then load them into your script using the . command, like this:
# . /path/to/functions.sh

function pause() {
   read -p "Press [Enter] key to continue..."
}

function customPause() {
   read -p "$*"
}
