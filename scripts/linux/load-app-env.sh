#!/bin/bash
# Load the app environment
# This script is not meant to be launched just itself.
# command line arguments :
# APP_ENV : the desired environment => not used, but required because of DOCKER_IMAGE_TAG argument
# (optional) DOCKER_IMAGE_TAG the docker image tag
# example : load-app-env.sh uat (paiement-bdm-api)

. ./common-functions.sh

ALLOWED_APP_ENVIRONMENTS=uat,prod
if [ -z "$1" ]; then
	echo "ERROR : You must specify an environment. Allowed app environments are : $ALLOWED_APP_ENVIRONMENTS."
	exit 1
fi

export APP_ENV=$1
export HOST_PORT=-1
export CONTAINER_PORT=9000
export DOCKER_IMAGE_TAG=chrisnb3/paiement-bdm-api

if ! [ -z "$2" ]; then
	DOCKER_IMAGE_TAG=$2
fi

if [ $APP_ENV == "uat" ]; then HOST_PORT=9100; fi
if [ $APP_ENV == "prod" ]; then HOST_PORT=9000; fi
if [ $HOST_PORT -eq -1 ]
	then
		echo "ERROR : You must specify an environment. Allowed app environments are : $ALLOWED_APP_ENVIRONMENTS."
		exit 1001
fi

echo "Environment $APP_ENV successfuly loaded."
echo "Host port: $HOST_PORT"
