@echo off
REM Builds the app as a Docker image
REM command line arguments: APP_ENV: the desired environment => not used, but required because of DOCKER_IMAGE_TAG argument
REM (optional) DOCKER_IMAGE_TAG the docker image tag
REM example:    build-app.cmd uat (paiement-bdm-api)

REM Load the environment variables
call load-app-env.cmd %*
if NOT %errorlevel% == 0 (
  echo ERROR : An error occured while loading app environment. Aborting.
  pause
  exit /b %errorlevel%
)

call change-dir.cmd
echo Current directory: "%cd%"

REM Package the app
@echo on
call mvn clean package -DskipTests
@echo off
if NOT %errorlevel% == 0 (
  echo ERROR : Error while running mvn clean package. Aborting.
  pause
  exit /b 1111
)

REM Build the Docker image
@echo on
call docker build --tag=%DOCKER_IMAGE_TAG% .
@echo off
if NOT %errorlevel% == 0 (
  echo ERROR : Error while running docker build. Aborting.
  pause
  exit /b 1102
)