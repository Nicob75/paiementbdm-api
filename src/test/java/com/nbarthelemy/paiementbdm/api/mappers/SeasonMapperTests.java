package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.SeasonEntity;
import com.nbarthelemy.paiementbdm.api.model.Season;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith( SpringExtension.class )
@SpringBootTest()
class SeasonMapperTests {

	@Autowired
	private SeasonMapper seasonMapper;

	@Test
	void seasonMapperTest() {
		Season season = new Season();
		season.setYearFrom(2020);
		season.setYearTo(2021);
		season.setStartDate(new Date());
		season.setEndDate(new Date());
		SeasonEntity seasonEntity = seasonMapper.modelToEntity(season);

		assertThat(season.getYearFrom()).isEqualTo( seasonEntity.getYearFrom() );
		assertThat(season.getYearTo()).isEqualTo( seasonEntity.getYearTo() );
		assertThat(season.getStartDate()).isEqualTo( seasonEntity.getStartDate() );
		assertThat(season.getEndDate()).isEqualTo( seasonEntity.getEndDate() );
	}

}
