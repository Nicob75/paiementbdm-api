SET @current_season = 10;
SET @current_season_foot11 = 2;
SET @current_season_foot7 = 2;

CREATE TABLE IF NOT EXISTS app_parameters (
                                  app_key varchar(64) DEFAULT NULL,
                                  app_value varchar(256) DEFAULT NULL,
                                  CONSTRAINT key_UNIQUE UNIQUE (app_key)
);

TRUNCATE TABLE app_parameters;

INSERT INTO app_parameters
VALUES ('CURRENT_SEASON', @current_season);

INSERT INTO app_parameters
(app_key,
 app_value)
VALUES
    ('CURRENT_DIVISION_FOOT_11',
     @current_season_foot11),
    ('CURRENT_DIVISION_FOOT_7',
     @current_season_foot7);
