package com.nbarthelemy.paiementbdm.api.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("spring.profiles")
@Getter
@Setter
@ToString
public class ProfilesProperties {
    private String active;
}
