package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.AppParameterEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppParameterRepository extends JpaRepository<AppParameterEntity, String> {
	
}
