package com.nbarthelemy.paiementbdm.api.repository;

import com.nbarthelemy.paiementbdm.api.entities.UserReadFeatureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserReadFeatureRepository extends JpaRepository<UserReadFeatureEntity, Integer> {
}
