package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.SeasonPlayerMapper;
import com.nbarthelemy.paiementbdm.api.model.EventPlayer;
import com.nbarthelemy.paiementbdm.api.model.Season;
import com.nbarthelemy.paiementbdm.api.model.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.repository.SeasonPlayerRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SeasonPlayerService {

	private final SeasonPlayerRepository seasonPlayerRepository;
	private final SeasonPlayerMapper seasonPlayerMapper;
	private final SeasonService seasonService;
	private final EventPlayerService eventPlayerService;

	public SeasonPlayerService(SeasonPlayerRepository seasonPlayerRepository,
							   SeasonPlayerMapper seasonPlayerMapper,
							   SeasonService seasonService,
							   EventPlayerService eventPlayerService ) {
		this.seasonPlayerRepository = seasonPlayerRepository;
		this.seasonPlayerMapper = seasonPlayerMapper;
		this.seasonService = seasonService;
		this.eventPlayerService = eventPlayerService;
	}

	/**
	 * Get a SeasonPlayer by Id
	 *
	 * @param id The SeasonPlayer
	 * @return The SeasonPlayer if found, else null
	 */
	public SeasonPlayer get(int id) {
		SeasonPlayerEntity seasonPlayerEntity = this.seasonPlayerRepository.findById(id).orElse(null);
		return this.seasonPlayerMapper.entityToModel(seasonPlayerEntity);
	}

	/**
	 * Get a SeasonPlayer (by Id)
	 *
	 * @param seasonPlayer The SeasonPlayer
	 * @return The SeasonPlayer if found, else null
	 */
	public SeasonPlayer get(SeasonPlayer seasonPlayer) {
		return this.get(seasonPlayer.getId());
	}

	/**
	 * Get a SeasonPlayer by SeasonId
	 *
	 * @param seasonId The Season Id
	 * @return A list of SeasonPlayer
	 */
	public List<SeasonPlayer> getBySeason(int seasonId) {
		List<SeasonPlayerEntity> seasonPlayerEntities = this.seasonPlayerRepository.findAllBySeasonId(seasonId);
		return this.seasonPlayerMapper.entitiesToModels(seasonPlayerEntities);
	}

	/**
	 * Get a SeasonPlayer by PlayerId
	 *
	 * @param playerId The Player Id
	 * @return A list of SeasonPlayer
	 */
	public List<SeasonPlayer> getByPlayer(int playerId) {
		List<SeasonPlayerEntity> seasonPlayerEntities = this.seasonPlayerRepository.findAllByPlayerId(playerId);
		return this.seasonPlayerMapper.entitiesToModels(seasonPlayerEntities);
	}

	public List<Season> getSeasonsByPlayer(int playerId) {
		List<SeasonPlayer> seasonPlayerEntities = this.getByPlayer(playerId);
		return seasonPlayerEntities.stream()
				.map(sp -> this.seasonService.get(sp.getSeasonId()))
				.collect(Collectors.toList());
	}


	/**
	 * Get SeasonPlayer by SeasonId and PlayerId
	 *
	 * @param seasonId The seasonId
	 * @param playerId The playerId
	 * @return The retrieved SeasonPlayer or null
	 */
	public SeasonPlayer get(int seasonId, int playerId) {
		SeasonPlayerEntity seasonPlayerEntity = this.seasonPlayerRepository.findBySeasonIdAndPlayerId(seasonId, playerId);
		return this.seasonPlayerMapper.entityToModel(seasonPlayerEntity);
	}

	/***
	 * Create the SeasonPlayer passed in parameter if it does not exist
	 * Additional existence criteria: same seasonId and playerId
	 *
	 * @param seasonPlayer The SeasonPlayer to be created
	 * @return The created SeasonPlayer
	 * @throws Exception if the SeasonPlayer already exists
	 */
	public SeasonPlayer create(SeasonPlayer seasonPlayer) throws Exception {
		if (this.get(seasonPlayer) != null) {
			throw new Exception("SeasonPlayer with Id '" + seasonPlayer.getId() + "' already exists ! Can't create.");
		}

		// check seasonId and playerId
		SeasonPlayer checkedSeasonPlayerEntity = this.get(seasonPlayer.getSeasonId(), seasonPlayer.getPlayerId());
		if (checkedSeasonPlayerEntity != null) {
			throw new DataIntegrityViolationException(
					"An SeasonPlayer (Id: " + checkedSeasonPlayerEntity.getId() + ") with seasonId '"
					+ seasonPlayer.getSeasonId() + "' and playerId '" + seasonPlayer.getPlayerId()
					+ " already exists ! Can't create.");
		}

		SeasonPlayerEntity seasonPlayerEntityToCreate = this.seasonPlayerMapper.modelToEntity(seasonPlayer);
		SeasonPlayerEntity createdSeasonPlayerEntity = this.seasonPlayerRepository.save(seasonPlayerEntityToCreate);
		return this.seasonPlayerMapper.entityToModel(createdSeasonPlayerEntity);
	}

	/**
	 * Update the SeasonPlayer passed in parameter if it exists.
	 * If no Id has been passed, we throw an exception.
	 *
	 * @param seasonPlayer The SeasonPlayer to be updated
	 * @return The updated SeasonPlayer
	 * @throws Exception if the SeasonPlayer does not exists
	 */
	public SeasonPlayer update(SeasonPlayer seasonPlayer) throws Exception {
		if (get(seasonPlayer) == null) {
			throw new Exception("SeasonPlayer with id: '" + seasonPlayer.getId() + "' does not exist ! Can't update.");
		}

		SeasonPlayer checkedSeasonPlayerEntity = this.get(seasonPlayer.getSeasonId(), seasonPlayer.getPlayerId());
		if (checkedSeasonPlayerEntity == null) {
			throw new Exception("No SeasonPlayer with seasonId '" + seasonPlayer.getSeasonId()
					+ "' and playerId '" + seasonPlayer.getPlayerId() + " exist ! Can't update.");
		}

		SeasonPlayerEntity seasonPlayerEntityToUpdate = this.seasonPlayerMapper.modelToEntity(seasonPlayer);
		SeasonPlayerEntity updatedSeasonPlayerEntity = this.seasonPlayerRepository.save(seasonPlayerEntityToUpdate);
		return this.seasonPlayerMapper.entityToModel(updatedSeasonPlayerEntity);
	}

	/**
	 * Delete the SeasonPlayer by id
	 *
	 * @param id The id
	 */
	public void delete(int id) {
		seasonPlayerRepository.deleteById(id);
	}

	/**
	 * Delete the SeasonPlayer by playerId
	 *
	 * @param playerId The player id
	 */
	@Transactional
	public void deleteBySeasonAndPlayerId(int seasonId, int playerId) {
		if ( playerHasAnExistingGameThisSeason(seasonId, playerId) ) {
			throw new DataIntegrityViolationException(
					"Player with id: '" + playerId + "' has an existing game this season ! Can't delete.");
		} else {
			seasonPlayerRepository.deleteBySeasonIdAndPlayerId(seasonId, playerId);
		}
	}

	public boolean playerHasAnExistingGameThisSeason(int seasonId, int playerId) {
		List<EventPlayer> eventPlayerEntities = this.eventPlayerService.getBySeason(seasonId);
		return eventPlayerEntities.stream()
				.anyMatch(ep -> ep.getPlayerId() == playerId);
	}
	
//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}