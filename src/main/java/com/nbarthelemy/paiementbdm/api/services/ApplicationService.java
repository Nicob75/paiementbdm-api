package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.properties.DataSourceProperties;
import com.nbarthelemy.paiementbdm.api.properties.ProfilesProperties;
import com.nbarthelemy.paiementbdm.api.repository.EventRepository;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService {

	private ProfilesProperties profilesProperties;
	private DataSourceProperties dataSourceProperties;
	private EventRepository eventRepository;

	public ApplicationService(
			ProfilesProperties profilesProperties,
			DataSourceProperties dataSourceProperties,
			EventRepository eventRepository
	) {
		this.profilesProperties = profilesProperties;
		this.dataSourceProperties = dataSourceProperties;
		this.eventRepository = eventRepository;
	}

	/**
	 *
	 *
	 * @return The active profile
	 */
	public String getActiveProfile() {
		return profilesProperties.getActive();
	}

	/**
	 *
	 *
	 * @return The datasource url
	 */
	public String getDataSourceUrl() {
		return dataSourceProperties.getUrl();
	}

	public int refreshDB() {
		return this.eventRepository.refreshDB();
	}
}