package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.UserEntity;
import com.nbarthelemy.paiementbdm.api.mappers.UserMapper;
import com.nbarthelemy.paiementbdm.api.model.User;
import com.nbarthelemy.paiementbdm.api.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

	private final UserRepository userRepository;
	private final UserMapper userMapper;

	public UserService(UserRepository userRepository,
                       UserMapper userMapper) {
		this.userRepository = userRepository;
		this.userMapper = userMapper;
	}

	/**
	 * Get a User by Id
	 *
	 * @param id The User Id
	 * @return The User if found, else null
	 */
	public User get(long id) {
		UserEntity userEntity = this.userRepository.findById(id).orElse(null);
		return this.userMapper.entityToModel(userEntity);
	}

	/**
	 * Get a User (by Id)
	 *
	 * @param user The User Id
	 * @return The User if found, else null
	 */
	public User get(User user) {
		return this.get(user.getId());
	}

	/**
	 * Get all the Users
	 * @return
	 */
	public List<User> getAll() {
		List<UserEntity> userEntities = this.userRepository.findAll();
		return this.userMapper.entitiesToModels(userEntities);
	}

}