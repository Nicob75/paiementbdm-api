package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.entities.PlayerEntity;
import com.nbarthelemy.paiementbdm.api.mappers.PlayerMapper;
import com.nbarthelemy.paiementbdm.api.model.Player;
import com.nbarthelemy.paiementbdm.api.model.Season;
import com.nbarthelemy.paiementbdm.api.model.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.repository.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerService {

	private final PlayerRepository playerRepository;
	private final PlayerMapper playerMapper;

	private final SeasonPlayerService seasonPlayerService;

	public PlayerService(PlayerRepository playerRepository,
						 PlayerMapper playerMapper,
						 SeasonPlayerService seasonPlayerService) {
		this.playerRepository = playerRepository;
		this.playerMapper = playerMapper;
		this.seasonPlayerService = seasonPlayerService;
	}

	/**
	 * Get all the Player
	 *
	 * @return All the Player
	 */
	public List<Player> getAll() {
		List<PlayerEntity> playerEntities = this.playerRepository.findAll();
		List<Player> players = this.playerMapper.entitiesToModels(playerEntities);
		players.forEach(this::retrieveSeasonsForPlayer);
		return players;
	}

	/**
	 * Get all the Player by Season
	 *
	 * @return All the Player by Season
	 */
	public List<Player> getBySeason(int id) {
		List<SeasonPlayer> seasonPlayerEntities = this.seasonPlayerService.getBySeason(id);
		List<Player> players = getAll().stream()
				.filter(p -> seasonPlayerEntities.stream()
						.anyMatch(sp -> sp.getPlayerId() == p.getId())
				)
				.collect(Collectors.toList());
		players.forEach(this::retrieveSeasonsForPlayer );
		return players;
	}

	public void retrieveSeasonsForPlayer(Player player) {
		List<Season> seasons = this.seasonPlayerService.getSeasonsByPlayer(player.getId());
		player.setSeasons(seasons);
	}

	/**
	 * Get a Player by Id
	 *
	 * @param id The Player Id
	 * @return The Player if found, else null
	 */
	public Player get(int id) {
		PlayerEntity playerEntity = this.playerRepository.findById(id).orElse(null);
		return this.playerMapper.entityToModel(playerEntity);
	}

	/**
	 * Get a Player (by Id)
	 *
	 * @param player The Player
	 * @return The Player if found, else null
	 */
	public Player get(Player player) {
		PlayerEntity playerEntity = this.playerRepository.findById(player.getId()).orElse(null);
		return this.playerMapper.entityToModel(playerEntity);
	}

	/***
	 * Create the Player passed in parameter if it does not exist
	 * Additional existence criteria: same bdmId
	 *
	 * @param player The Player to be created
	 * @return The created Player
	 * @throws Exception if the Player already exists (checked by bdmId)
	 */
	public Player create(Player player) throws Exception {
		Player existingPlayer = get(player);

		if (existingPlayer != null) {
			throw new Exception("Player with Id '" + player.getId() + "' already exists ! Can't create.");
		}

		// check bdmId
		List<Player> playerList = getAll();
		Player playerBDMId = playerList.stream()
				.filter(p -> p.getBdmId() == player.getBdmId())
				.findAny().orElse(null);
		if (playerBDMId != null) {
			throw new Exception("A Player (Id: " + playerBDMId.getId() + ") with bdmId '" + player.getBdmId()
					+ "' already exists ! Can't create.");
		}

		PlayerEntity playerEntityToCreate = this.playerMapper.modelToEntity(player);
		PlayerEntity createdPlayerEntity = this.playerRepository.save(playerEntityToCreate);
		return this.playerMapper.entityToModel(createdPlayerEntity);
	}

	/**
	 * Update the Player passed in parameter if it exists.
	 * If no Id has been passed, we check if the bdmId already exists and update that Player if it's the case.
	 *
	 * @param player The Player to be updated
	 * @return The updated Player
	 * @throws Exception if the Player does not exists (checked by bdmId)
	 */
	public Player update(Player player) throws Exception {
		if (get(player) == null) {
			throw new Exception("Player with id: '" + player.getId() + "' does not exist ! Can't update.");
		}

		PlayerEntity playerEntityToUpdate = this.playerMapper.modelToEntity(player);
		PlayerEntity updatedPlayerEntity = this.playerRepository.save(playerEntityToUpdate);
		return this.playerMapper.entityToModel(updatedPlayerEntity);
	}

	/**
	 * Delete the Player by id
	 *
	 * @param id The id
	 */
	public void delete(int id) {
		this.playerRepository.deleteById(id);
	}

//	public boolean deleteById() {
//		playerRepository.deleteById();
//	}
	
//	private Player getTestPlayer() {
//		return new Player(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}