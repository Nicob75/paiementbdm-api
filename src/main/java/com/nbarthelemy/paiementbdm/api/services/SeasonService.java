package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.config.AppParametersConfig;
import com.nbarthelemy.paiementbdm.api.entities.SeasonEntity;
import com.nbarthelemy.paiementbdm.api.mappers.SeasonMapper;
import com.nbarthelemy.paiementbdm.api.model.Season;
import com.nbarthelemy.paiementbdm.api.repository.SeasonRepository;
import org.springframework.stereotype.Service;

@Service
public class SeasonService {

	private final SeasonRepository seasonRepository;
	private final SeasonMapper seasonMapper;
	private final AppParametersConfig appParametersConfig;

	public SeasonService(SeasonRepository seasonRepository,
						 SeasonMapper seasonMapper,
						 AppParametersConfig appParametersConfig) {
		this.seasonRepository = seasonRepository;
		this.seasonMapper = seasonMapper;
		this.appParametersConfig = appParametersConfig;
	}

	/**
	 * Get a Season by Id
	 *
	 * @param id The Season Id
	 * @return The Season if found, else null
	 */
	public Season get(int id) {
		SeasonEntity seasonEntity = this.seasonRepository.findById(id).orElse(null);
		return this.seasonMapper.entityToModel(seasonEntity);
	}

	/**
	 * Get a Season (by Id)
	 *
	 * @param season The Season Id
	 * @return The Season if found, else null
	 */
	public Season get(Season season) {
		return this.get(season.getId());
	}

	/***
	 * Create the Season passed in parameter if it does not exist
	 *
	 * @param season The Season to be created
	 * @return The created Season
	 * @throws Exception if the Season already exists
	 */
	public Season create(Season season) throws Exception {
		Season existingSeasonEntity = this.get(season);

		if (existingSeasonEntity != null) {
			throw new Exception("Season with Id '" + season.getId() + "' already exists ! Can't create.");
		}

		SeasonEntity seasonEntityToCreate = this.seasonMapper.modelToEntity(season);
		SeasonEntity createdSeasonEntity = this.seasonRepository.save(seasonEntityToCreate);
		return this.seasonMapper.entityToModel(createdSeasonEntity);
	}

	/**
	 * Update the Season passed in parameter if it exists
	 *
	 * @param season The Season to be updated
	 * @return The updated Season
	 * @throws Exception if the Season does not exists
	 */
	public Season update(Season season) throws Exception {
		if (this.get(season) == null) {
			throw new Exception("Season with id: '" + season.getId() + "' does not exist ! Can't update.");
		}

		SeasonEntity seasonEntityToUpdate = this.seasonMapper.modelToEntity(season);
		SeasonEntity updatedSeasonEntity = this.seasonRepository.save(seasonEntityToUpdate);
		return this.seasonMapper.entityToModel(updatedSeasonEntity);
	}

	public int getCurrentSeasonId() {
		String currentSeasonStr = this.appParametersConfig.getAppParameters().get( AppParametersConfig.CURRENT_SEASON );
		return Integer.parseInt( currentSeasonStr );
	}

	public Season getCurrentSeason() {
		int currentSeasonId = getCurrentSeasonId();
		return this.get( currentSeasonId );
	}

//	private Event getTestEvent() {
//		return new Event(
//				9,
//				"BDM FC A",
//				"5",
//				"2",
//				"Paris Lutece",
//				new Date(System.currentTimeMillis()),
//				7,
//				"Foot a 7",
//				"1"
//			);
//	}

}