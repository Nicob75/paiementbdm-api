package com.nbarthelemy.paiementbdm.api.services;

import com.nbarthelemy.paiementbdm.api.mappers.FeatureMapper;
import com.nbarthelemy.paiementbdm.api.mappers.UserReadFeatureMapper;
import com.nbarthelemy.paiementbdm.api.model.Feature;
import com.nbarthelemy.paiementbdm.api.model.UserReadFeature;
import com.nbarthelemy.paiementbdm.api.repository.FeatureRepository;
import com.nbarthelemy.paiementbdm.api.repository.UserReadFeatureRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeatureService {
    private final FeatureRepository featureRepository;
    private final FeatureMapper featureMapper;
    private final UserReadFeatureRepository userReadFeatureRepository;
    private final UserReadFeatureMapper userReadFeatureMapper;

    public FeatureService(FeatureRepository featureRepository,
                          FeatureMapper featureMapper,
                          UserReadFeatureRepository userReadFeatureRepository,
                          UserReadFeatureMapper userReadFeatureMapper) {
        this.featureRepository = featureRepository;
        this.featureMapper = featureMapper;
        this.userReadFeatureRepository = userReadFeatureRepository;
        this.userReadFeatureMapper = userReadFeatureMapper;
    }

    public List<Feature> getFeatures() {
        var featuresEntities = featureRepository.findAll();
        var features = featureMapper.entitiesToModels( featuresEntities );
        features.sort( FeatureService::compare );
        return features;
    }

    public List<Feature> getUserUnreadFeatures(long userId) {
        var featuresEntities = featureRepository.getUserUnreadFeatures(userId);
        var features = featureMapper.entitiesToModels(featuresEntities);
        features.sort( FeatureService::compare );
        return features;
    }

    public void markAsRead(long userId, List<Integer> featureIds) {
        for (int featureId : featureIds) {
            var userReadFeature = new UserReadFeature(userId, featureId);
            userReadFeatureRepository.save(userReadFeatureMapper.modelToEntity(userReadFeature));
        }
    }

    private static int compare(Feature f1, Feature f2) {
        return f2.getCreatedDate()
                 .before( f1.getCreatedDate() ) ? 1 : -1;
    }
}
