package com.nbarthelemy.paiementbdm.api.security.jwt;

import com.nbarthelemy.paiementbdm.api.config.PaiementBdmConfig;
import com.nbarthelemy.paiementbdm.api.security.service.UserDetailsImpl;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    private PaiementBdmConfig paiementBdmConfig;

    public JwtUtils(
            PaiementBdmConfig paiementBdmConfig
    ) {
        this.paiementBdmConfig = paiementBdmConfig;
        System.out.println();
    }

    public String generateJwtToken(Authentication authentication) {

        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
        String strJwtExpirationMs = paiementBdmConfig.getJwtExpirationMs();
        int jwtExpirationMs = Integer.parseInt(strJwtExpirationMs);

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, paiementBdmConfig.getJwtSecret())
                .compact();
    }

    String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(paiementBdmConfig.getJwtSecret()).parseClaimsJws(token).getBody().getSubject();
    }

    boolean validateJwtToken(String authToken, HttpServletRequest request) {
        try {
            Jwts.parser().setSigningKey(paiementBdmConfig.getJwtSecret()).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
            request.setAttribute("expired", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }
}
