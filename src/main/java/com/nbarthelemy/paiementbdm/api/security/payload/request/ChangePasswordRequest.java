package com.nbarthelemy.paiementbdm.api.security.payload.request;

import lombok.Getter;
import lombok.Setter;

import jakarta.validation.constraints.NotBlank;

@Getter
@Setter
public class ChangePasswordRequest {
	@NotBlank
	private String username;

	@NotBlank
	private String password;
}
