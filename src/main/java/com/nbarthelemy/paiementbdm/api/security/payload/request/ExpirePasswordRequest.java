package com.nbarthelemy.paiementbdm.api.security.payload.request;

import lombok.Getter;
import lombok.Setter;

import jakarta.validation.constraints.NotBlank;

@Getter
@Setter
public class ExpirePasswordRequest {
	@NotBlank
	private String username;
}
