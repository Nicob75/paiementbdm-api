package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.model.SeasonPlayer;
import com.nbarthelemy.paiementbdm.api.entities.SeasonPlayerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SeasonPlayerMapper {
    @Mapping(target = "id", ignore = true)
    SeasonPlayerEntity modelToEntity(SeasonPlayer seasonPlayer);
    SeasonPlayer entityToModel(SeasonPlayerEntity seasonPlayerEntity);

    List<SeasonPlayerEntity> modelsToEntities(List<SeasonPlayer> seasonPlayers);
    List<SeasonPlayer> entitiesToModels(List<SeasonPlayerEntity> seasonPlayersEntities);
}
