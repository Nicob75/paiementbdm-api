package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.SeasonEntity;
import com.nbarthelemy.paiementbdm.api.model.Season;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SeasonMapper {
    @Mapping(target = "id", ignore = true)
    SeasonEntity modelToEntity(Season seasonDTOs);
    Season entityToModel(SeasonEntity seasonDTOs);

    List<SeasonEntity> modelsToEntities(List<Season> seasons);
    List<Season> entitiesToModels(List<SeasonEntity> seasonEntities);
}
