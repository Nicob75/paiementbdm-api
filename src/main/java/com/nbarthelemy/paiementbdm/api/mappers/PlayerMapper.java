package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.model.Player;
import com.nbarthelemy.paiementbdm.api.entities.PlayerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlayerMapper {
    @Mapping(target = "id", ignore = true)
    PlayerEntity modelToEntity(Player player);
    Player entityToModel(PlayerEntity playerEntity);

    List<PlayerEntity> modelsToEntities(List<Player> players);
    List<Player> entitiesToModels(List<PlayerEntity> playersEntities);
}
