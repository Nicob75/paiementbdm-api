package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.FeatureEntity;
import com.nbarthelemy.paiementbdm.api.model.Feature;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FeatureMapper {
    FeatureEntity modelToEntity(Feature feature);
    Feature entityToModel(FeatureEntity featureEntity);

    List<FeatureEntity> modelsToEntities(List<Feature> features);
    List<Feature> entitiesToModels(List<FeatureEntity> featureEntities);
}
