package com.nbarthelemy.paiementbdm.api.mappers;

import com.nbarthelemy.paiementbdm.api.entities.ERole;
import com.nbarthelemy.paiementbdm.api.entities.RoleEntity;
import com.nbarthelemy.paiementbdm.api.entities.UserEntity;
import com.nbarthelemy.paiementbdm.api.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    @Mapping(target = "id", ignore = true)
    UserEntity modelToEntity(User user);
    @Mapping(source = "roleEntities", target = "roles", qualifiedByName = "mapRole")
    User entityToModel(UserEntity userEntity);

    List<UserEntity> modelsToEntities(List<User> users);
    List<User> entitiesToModels(List<UserEntity> userEntities);

    @Named("mapRole")
    default ERole mapRole(RoleEntity roleEntity) {
        return roleEntity.getName();
    }
}
