package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "seasonplayers")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SeasonPlayerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="seasonid")
	private int seasonId;
	@Column(name="playerid")
	private int playerId;

	public SeasonPlayerEntity(int seasonId, int playerId) {
		super();
		this.seasonId = seasonId;
		this.playerId = playerId;
	}

}
