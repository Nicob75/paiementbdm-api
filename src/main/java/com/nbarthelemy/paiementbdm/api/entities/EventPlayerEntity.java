package com.nbarthelemy.paiementbdm.api.entities;

import lombok.*;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "eventplayers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EventPlayerEntity extends AuditableEntity<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="eventid")
	private int eventId;
	@Column(name="playerid")
	private int playerId;
	@Column(name="payment")
	private float payment;
	@Column(name="keeper")
	private float keeper;
}
