package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;
import java.util.Date;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "seasons")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class SeasonEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="yearfrom")
	private int yearFrom;
	@Column(name="yearto")
	private int yearTo;
	@Column(name="startdate")
	private Date startDate;
	@Column(name="enddate")
	private Date endDate;

	public SeasonEntity(int yearFrom, int yearTo, Date startDate, Date endDate) {
		super();
		this.yearFrom = yearFrom;
		this.yearTo = yearTo;
		this.startDate = startDate;
		this.endDate = endDate;
	}
}
