package com.nbarthelemy.paiementbdm.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import jakarta.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "players")
@Getter
@Setter
@NoArgsConstructor
@ToString
//@AllArgsConstructor
public class PlayerEntity extends AuditableEntity<String> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	@Column(name="bdmid")
	private int bdmId;
	@Column(name="firstname")
	private String firstname;
	@Column(name="lastname")
	private String lastname;
	@Column(name="jerseynumber")
	private int jerseyNumber;

	public PlayerEntity(int bdmId, String firstname, String lastname, int jerseyNumber) {
		super();
		this.bdmId = bdmId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.jerseyNumber = jerseyNumber;
	}
}
