package com.nbarthelemy.paiementbdm.api.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "app_parameters")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class AppParameterEntity {
    @Id
    private String appKey;
    private String appValue;
}
