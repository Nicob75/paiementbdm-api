package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.model.Feature;
import com.nbarthelemy.paiementbdm.api.services.FeatureService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/features")
public class FeatureController {
    private final FeatureService featureService;

    public FeatureController(
            FeatureService featureService
    ) {
        this.featureService = featureService;
    }

    @Secured({"ROLE_USER", "ROLE_MANAGER", "ROLE_ADMIN"})
    @ResponseBody
    @GetMapping(value = "unread/user/{id}")
    public List<Feature> getUserUnreadFeatures(@PathVariable("id") long userId) {
        return this.featureService.getUserUnreadFeatures(userId);
    }

    @Secured({"ROLE_USER", "ROLE_MANAGER", "ROLE_ADMIN"})
    @ResponseBody
    @PostMapping(value = "read/user/{id}")
    public void markAsRead(@PathVariable("id") long userId, @RequestBody List<Integer> featureIds) {
        this.featureService.markAsRead(userId, featureIds);
    }

    @GetMapping()
    public List<Feature> getFeatures() {
        return this.featureService.getFeatures();
    }

}
