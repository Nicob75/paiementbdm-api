package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.services.ApplicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
@RequestMapping("/application")
public class ApplicationController {

	private final ApplicationService applicationService;

	public ApplicationController(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}

    @ResponseBody
    @GetMapping(value = "/profile")
    public ResponseEntity<String> getEnvironment() {
		return ResponseEntity.ok(this.applicationService.getActiveProfile());
	}

    @ResponseBody
    @GetMapping("/refresh/db")
    public ResponseEntity<Integer> refreshDB() {
        return ResponseEntity.ok(this.applicationService.refreshDB());
    }
}
