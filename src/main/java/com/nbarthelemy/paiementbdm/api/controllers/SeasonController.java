package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.model.Season;
import com.nbarthelemy.paiementbdm.api.entities.SeasonEntity;
import com.nbarthelemy.paiementbdm.api.mappers.SeasonMapper;
import com.nbarthelemy.paiementbdm.api.repository.SeasonRepository;
import com.nbarthelemy.paiementbdm.api.services.SeasonService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Controller
@CrossOrigin
@RequestMapping("/season")
public class SeasonController {

	private final SeasonService seasonService;
	private final SeasonMapper seasonMapper;
	private final SeasonRepository seasonRepository;

	public SeasonController(SeasonService seasonService,
							SeasonMapper seasonMapper,
							SeasonRepository seasonRepository) {
		this.seasonService = seasonService;
		this.seasonMapper = seasonMapper;
		this.seasonRepository = seasonRepository;
	}

    @ResponseBody
    @GetMapping(value = "/all")
    public List<Season> getAll() {
		List<SeasonEntity> seasonEntities = this.seasonRepository.findAll();
		return this.seasonMapper.entitiesToModels(seasonEntities);
	}

	@ResponseBody
	@GetMapping(value = "/{id}")
	public Season get(@PathVariable("id") int id) {
		Season season = this.seasonService.get(id);
		if (season != null) {
			return season;
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EventPlayer not found");
	}

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@PostMapping(value = "")
	public Season create(@RequestBody Season season) throws Exception {
		return this.seasonService.create(season);
	}

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@PutMapping(value = "/{id}")
	public Season update(@PathVariable("id") int id, @RequestBody Season season) throws Exception {
		season.setId(id);
		return this.seasonService.update(season);
	}

	@ResponseBody
	@GetMapping(value = "/current")
	public Season getCurrentSeason() {
		return this.seasonService.getCurrentSeason();
	}
	
	/*private Event getTestEvent() {
		return new Event(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
