package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.model.User;
import com.nbarthelemy.paiementbdm.api.services.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
    private UserService userService;

    public UserController(
            UserService userService
    ) {
        this.userService = userService;
    }

    @Secured({"ROLE_ADMIN"})
    @ResponseBody
    @GetMapping(value = "/{id}")
    public User get(@PathVariable("id") long id) {
        return this.userService.get(id);
    }

    @Secured({"ROLE_ADMIN"})
    @ResponseBody
    @GetMapping(value = "/all")
    public List<User> getUsers() {
        return this.userService.getAll();
    }
}
