package com.nbarthelemy.paiementbdm.api.controllers;

import com.nbarthelemy.paiementbdm.api.model.Player;
import com.nbarthelemy.paiementbdm.api.services.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/player")
public class PlayerController {
	
	private final PlayerService playerService;
	
	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}

    @ResponseBody
    @GetMapping(value = "/all")
    public List<Player> getAll() {
		return this.playerService.getAll();
	}

	@ResponseBody
	@GetMapping(value = "season/{id}")
	public List<Player> getBySeason(@PathVariable("id") int seasonId) {
		return this.playerService.getBySeason(seasonId);
	}

	@ResponseBody
	@GetMapping(value = "/{id}")
	public Player get(@PathVariable("id") int id) {
		Player player = this.playerService.get(id);
		if (player != null) {
			return player;
		}
		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "EventPlayer not found");
	}

	@Secured({"ROLE_MANAGER"})
    @ResponseBody
    @PostMapping(value = "")
    public Player create(@RequestBody Player player) throws Exception {
        return this.playerService.create(player);
    }

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@PutMapping(value = "/{id}")
	public Player update(@PathVariable("id") int id, @RequestBody Player player) throws Exception {
		player.setId(id);
		return this.playerService.update(player);
	}

	@Secured({"ROLE_MANAGER"})
	@ResponseBody
	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable("id") int id) throws Exception {
		throw new Exception("Method forbidden for now !");
//		this.playerService.delete(id);
	}
	
	/*private Player getTestPlayer() {
		return new Player(
				5,
				"BDM FC A",
				"5",
				"2",
				"Paris Lutece",
				new Date(System.currentTimeMillis()),
				7,
				"Foot a 7",
				"1"
			);
	}*/
}
