package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class Feature {
    @Schema( accessMode = Schema.AccessMode.READ_ONLY )
    private int id;
    private String name;
    private String htmlDescription;
    private Date createdDate;
}
