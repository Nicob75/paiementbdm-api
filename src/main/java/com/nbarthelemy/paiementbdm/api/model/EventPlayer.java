package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode( callSuper = true )
@AllArgsConstructor
@Builder(toBuilder = true)
@Data
@NoArgsConstructor
public class EventPlayer extends Auditable<String> {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private int eventId;
	private int playerId;
	private float payment;
	private float keeper;

	public EventPlayer(int eventId, int playerId, float payment, float keeper) {
		super();
		this.eventId = eventId;
		this.playerId = playerId;
		this.payment = payment;
		this.keeper = keeper;
	}
}
