package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder(toBuilder = true)
@Data
@NoArgsConstructor
public class UserReadFeature {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private long userId;
	private int featureId;

	public UserReadFeature(long userId, int featureId) {
		super();
		this.userId = userId;
		this.featureId = featureId;
	}
}
