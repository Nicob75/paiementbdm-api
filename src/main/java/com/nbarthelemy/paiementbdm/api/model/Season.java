package com.nbarthelemy.paiementbdm.api.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode( callSuper = true )
@Data
public class Season extends Auditable<String> {
	@Schema( accessMode = Schema.AccessMode.READ_ONLY )
	private int id;
	private int yearFrom;
	private int yearTo;
	private Date startDate;
	private Date endDate;
}
