package com.nbarthelemy.paiementbdm.api.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "paiementbdm")
public class PaiementBdmConfig {
    private String jwtSecret;
    private String jwtExpirationMs;
}

