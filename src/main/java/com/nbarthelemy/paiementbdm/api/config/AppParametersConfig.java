package com.nbarthelemy.paiementbdm.api.config;

import com.nbarthelemy.paiementbdm.api.entities.AppParameterEntity;
import com.nbarthelemy.paiementbdm.api.repository.AppParameterRepository;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class AppParametersConfig {

    private final AppParameterRepository appParameterRepository;

    public static final String CURRENT_SEASON = "CURRENT_SEASON";
    public static final String CURRENT_DIVISION_FOOT_11 = "CURRENT_DIVISION_FOOT_11";
    public static final String CURRENT_DIVISION_FOOT_7 = "CURRENT_DIVISION_FOOT_7";

    @Getter
    private Map<String, String> appParameters;

    public AppParametersConfig( AppParameterRepository appParameterRepository ) {
        this.appParameterRepository = appParameterRepository;
        loadAppParameters();
    }

    private void loadAppParameters() {
        List<AppParameterEntity> allParams = this.appParameterRepository.findAll();
        appParameters = new HashMap<>();
        for ( AppParameterEntity appParam : allParams ) {
            appParameters.put( appParam.getAppKey(), appParam.getAppValue() );
        }
    }
}
