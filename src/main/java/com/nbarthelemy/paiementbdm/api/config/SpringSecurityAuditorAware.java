package com.nbarthelemy.paiementbdm.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef="auditorAware")
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Bean
    public AuditorAware<String> auditorAware() {
        return new SpringSecurityAuditorAware();
    }

    private static String getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) return null;
        return authentication.getName();
    }

    @NonNull
    @Override
    public Optional<String> getCurrentAuditor() {
        String user = getAuthenticatedUser();
        // Just return a string representing the username
        return Optional.ofNullable(user).filter(s -> !s.isEmpty());
    }

}
