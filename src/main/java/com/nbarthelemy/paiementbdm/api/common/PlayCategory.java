package com.nbarthelemy.paiementbdm.api.common;

public enum PlayCategory {
    FOOT7("Foot a 7"),
    FOOT11("Foot a 11");

    private String displayStr;

    PlayCategory(String displayStr) {
        this.displayStr = displayStr;
    }

    public String getDisplayStr() {
        return this.displayStr;
    }
}
