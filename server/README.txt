Paiement BDM API - README

[Deployment]
Build the application thanks to maven build
	Base directory: 	${project_loc:paiementbdm.api}
	Goals: 				clean install

Copy the paiement-bdm-api\ folder into Tomcat's webapps\ folder.
Copy the paiement-bdm-api.war file into Tomcat's webapps\paiement-bdm-api\ folder.

Thanks to the configuration files under conf\ folder (where this readme is), change Tomcat's server configuration in Tomcat's conf\ folder (you can use winmerge to see the difference).
Start the server.
